######################################
# target
######################################
TARGET = bootloader


######################################
# building variables
######################################
# optimization
# 	O0 -    Unoptimized ( faster compile time )
# 	O1 -    General optimizations no speed/size tradeoff
# 	O2 -    More aggressive size and speed optimization
# 	O3 -    Favor speed over size
# 	Os -    Favor size over speed
# 	Ofast - O3 plus inaccurate math
# 	Og -    Optimize for debugging experience
OPT ?= Os

#######################################
# paths
#######################################
# Build path
BUILD_DIR = build

######################################
# source
######################################
# C sources
C_SOURCES =  \
startup_stm32g431xx.c \
boot.c \
submodules/shared-utilities/sclib/image.c \
submodules/shared-utilities/sclib/crc.c \
submodules/shared-utilities/sclib/shared_ram.c

# ASM sources
ASM_SOURCES =

#######################################
# binaries
#######################################
EXPECTED_GCC_VERSION := 10.2.1
CC = arm-none-eabi-gcc
AS = arm-none-eabi-gcc -x assembler-with-cpp
CP = arm-none-eabi-objcopy
SZ = arm-none-eabi-size
READELF = arm-none-eabi-readelf
HEX = $(CP) -O ihex
BIN = $(CP) -O binary -S
MKDIR = mkdir -p

#######################################
# CFLAGS
#######################################
# cpu
CPU = -mcpu=cortex-m4

# fpu
FPU = -mfpu=fpv4-sp-d16

# float-abi
FLOAT-ABI = -mfloat-abi=hard

# mcu
MCU = $(CPU) -mthumb $(FPU) $(FLOAT-ABI)

# macros for gcc
# C defines
C_DEFS = -DSTM32G431xx

# C includes
C_INCLUDES = \
-Isubmodules/shared-utilities/CMSIS/Device/ST/STM32G4xx/Include \
-Isubmodules/shared-utilities/CMSIS/Include \
-Isubmodules/shared-utilities/sclib \
-I.

# compile gcc flags
CFLAGS = $(MCU) $(C_DEFS) $(C_INCLUDES) -$(OPT) -fdata-sections -ffunction-sections -g3 -gdwarf-2 -fstack-usage

# compiler warnings and errors
CFLAGS += -Wall -Wextra -Werror -Wshadow -Wdouble-promotion -Wformat=2 -Wformat-truncation=2 -Wundef -fno-common
CFLAGS += -Wno-unused-parameter

# Generate dependency information
CFLAGS += -MMD -MP -MF"$(@:%.o=%.d)"

# ensure reproducibility in file paths
CFLAGS += -fmacro-prefix-map="$(CURDIR)"=. -fdebug-prefix-map="$(CURDIR)"=.


#######################################
# LDFLAGS
#######################################
# link script
LDSCRIPT ?= bootloader.ld

# libraries
LIBS = -lc -lm -lnosys
LIBDIR =
LDFLAGS = $(MCU) -specs=nano.specs -T$(LDSCRIPT) $(LIBDIR) $(LIBS) -Wl,-Map=$(BUILD_DIR)/$(TARGET).map,--cref -Wl,--gc-sections -Wl,--build-id -Wl,--print-memory-usage

# default action: build all
all: version-check $(BUILD_DIR)/$(TARGET).elf build-id $(BUILD_DIR)/$(TARGET).hex $(BUILD_DIR)/$(TARGET).bin sha256
.PHONY: version-check sha256 setup

GCC_VERSION := $(strip $(shell $(CC) -dumpversion))
version-check:
ifneq ($(GCC_VERSION),$(EXPECTED_GCC_VERSION))
	$(error This project is intended to be compiled against $(EXPECTED_GCC_VERSION). You are using $(GCC_VERSION) ¯\_(ツ)_/¯)
else
	$(info Found matching GCC $(EXPECTED_GCC_VERSION))
endif

sha256: $(BUILD_DIR)/$(TARGET).elf $(BUILD_DIR)/$(TARGET).hex $(BUILD_DIR)/$(TARGET).bin
	@sha256sum $^

setup:
ifneq (${CI}, true)
	git submodule update --init --recursive
endif


#######################################
# build the application
#######################################
# list of objects
OBJECTS = $(addprefix $(BUILD_DIR)/,$(C_SOURCES:.c=.o))
vpath %.c $(sort $(dir $(C_SOURCES)))
# list of ASM program objects
OBJECTS += $(addprefix $(BUILD_DIR)/,$(ASM_SOURCES:.s=.o))
vpath %.s $(sort $(dir $(ASM_SOURCES)))

$(BUILD_DIR)/%.o: %.c Makefile
	@$(MKDIR) $(@D)
	@echo Compiling $*.c
	@$(CC) -c $(CFLAGS) -D__FILENAME__=\"$(notdir $<)\" -Wa,-a,-ad,-alms=$(BUILD_DIR)/$(<:.c=.lst) $*.c -o $@

$(BUILD_DIR)/%.o: %.s Makefile
	@$(MKDIR) $(@D)
	@echo Assembling $*.s
	@$(AS) -c $(CFLAGS) -D__FILENAME__=\"$(notdir $<)\" $*.s -o $@

$(BUILD_DIR)/$(TARGET).elf: $(OBJECTS) Makefile $(LDSCRIPT)
	@echo Linking $@
	@$(CC) $(OBJECTS) $(LDFLAGS) -o $@

$(BUILD_DIR)/%.hex: $(BUILD_DIR)/%.elf
	@$(MKDIR) $(@D)
	@echo Building $@
	@$(HEX) $< $@

$(BUILD_DIR)/%.bin: $(BUILD_DIR)/%.elf
	@$(MKDIR) $(@D)
	@echo Building $@
	@$(BIN) $< $@

.PHONY: elfsize
elfsize: $(BUILD_DIR)/$(TARGET).elf $(BUILD_DIR)/$(TARGET).hex $(BUILD_DIR)/$(TARGET).bin
	@$(SZ) $<

.PHONY: build-id
build-id: $(BUILD_DIR)/$(TARGET).elf
	@$(READELF) -n $< | grep "Build ID"


#######################################
# clean up
#######################################
.PHONY: clean
clean:
	-rm -fR $(BUILD_DIR)


#######################################
# dependencies
#######################################
SHELL:=/usr/bin/bash -O globstar
-include $(shell [[ -d $(BUILD_DIR) ]] && ls $(BUILD_DIR)/**/*.d)
