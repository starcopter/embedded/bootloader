/**
 * @file boot.c
 * @author Lasse Fröhner <lasse@starcopter.com>
 * @brief Immutable Bootloader
 * @date 2021-06-10
 *
 * @copyright Copyright (c) 2021 starcopter GmbH
 *
 * This software is distributed under the terms of the MIT License.
 *
 */

#include <inttypes.h>
#include <stddef.h>
#include <string.h>

#include <image.h>
#include <shared_ram.h>
#include <memory_map.h>

extern void                  _reset_context_on_boot(void);
extern ApplicationContext_t* _bootloader_get_launch_context(void);

int main(void) {
    _reset_context_on_boot();
    ApplicationContext_t* const ctx = _bootloader_get_launch_context();

    if (ctx->application == NULL) {
        // this is a cold boot
        ctx->application = (ImageHeader_t*) &__app_loader_start__;
    }

    while (ctx->application >= (ImageHeader_t*) &__app_loader_start__ &&
           ctx->application < (ImageHeader_t*) &__nvm_start__) {
        if (image_check(ctx->application, true, true) && ctx->boot_count < 3) {
            image_boot(ctx);
        }
        ctx->boot_count  = 0;
        ctx->application = (ImageHeader_t*) ((uint32_t) ctx->application + PAGE_SIZE);
    }

    return 1;
}
