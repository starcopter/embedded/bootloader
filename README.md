# Bootloader

This is the immutable bootloader, the first of three components comprising the device firmware update architecture outlined in [Memfault's blog article](https://interrupt.memfault.com/blog/device-firmware-update-cookbook#dfu-failures-should-not-brick-the-device).

The components are, or will be eventually:

0. the Bootloader (this project)
1. the [App Loader][]
2. the main application, e.g. [G4-Nucleo][]
3. the Loader Updater

> **Note**: As of now, this bootloader is only implemented for and tested on the STM32G431xx platform

  [App Loader]: https://gitlab.com/starcopter/embedded/app-loader
  [G4-Nucleo]: https://gitlab.com/starcopter/embedded/g4-nucleo

[[_TOC_]]

## Memory Layout

The bootloader is designed to be located at the very start of the application memory and is directly booted into by the Cortex-M4 core.

```
0x08020000 ┌─────────────────────┐
           │                     │
           .         126K        .         arbitrary number of page-aligned applications
           .  Application Space  . ◄─────  every application has to start with an image header
           .  Page (2K) Aligned  .         the bootloader discovers bootable apps and launches them
           │                     │
0x08000800 ├─────────────────────┤
           │    512 Bytes DCB    │ ◄─────  read-only device configuration block
0x08000600 ├─────────────────────┤
           │                     │         minimalistic immutable bootloader
           │   1.5K Bootloader   │ ◄─────  no external communication interfaces
           │                     │         can only interact with internal memory
0x08000000 └─────────────────────┘
```

Upon launch the bootloader will scan the application space from the start (`*0x08000800`) page by page for a valid [application](#applications) and jump into it.
Should that application crash before checking in (see [launch context](#launch-context-and-reboot-loops) below), it is given two more launch attempts (3 total),
before the bootloader discards the application and continues scanning for the next valid application entry.

In case the bootloader reaches the application space's end without finding a valid and stable application, it resets all [shared state](#shared-ram) and reboots.

### Device Configuration Block (DCB)

The bootloader shares a flash memory page with the device configuration block, described in [sclib/dcb.h][dcb.h].
Other than being on the same page, the bootloader and DCB are completely independent of each other.
See the [App Loader][] for details on the DCB contents and usage.

  [dcb.h]: https://gitlab.com/starcopter/embedded/shared-utilities/blob/fec945c7b70909782e1753f89619292c9b06782e/sclib/dcb.h

### Applications

An application has to start with an image header containing base metadata and a checksum for integrity checking, and end with another checksum at the end of the image.
Both these constructs are declared in [sclib/image.h][image.h].

```c
struct ImageHeader {
    /* header's header, this is expected not to change */
    const uint32_t        image_magic;   // should be IMAGE_MAGIC (0x3063737f)
    const uint32_t* const header_start;  // image base address, absolute
    const uint32_t* const vector_table;  // vector table address, absolute
    const uint32_t        header_size;   // total header size in bytes, e.g. sizeof(struct ImageHeaderVx)
    const uint32_t* const image_end;     // image end address (first non-image byte), absolute

    /* header's implementation specific body, (header_size - 24) bytes */
    const uint32_t image_body[];  // the bootloader does not care about this

    /* header's tail, has to be the last word */
    const uint32_t header_crc;  // CRC of the header up to here
};

/* Last word of an image, ENDS aligned at 8-byte boundary. Declared in Linker Script. */
extern uint32_t __image_crc;
```

The image header is built in an extensible way, to allow for future modifications.
The bootloader only interprets the header's first five and the single last word, allowing the version specific body to evolve over time.

The CRC algorithm shall be the standard CRC-32, with Python's [`binascii.crc32()`][crc32()] serving as a reference implementation.
Both checksums are calculated starting at the first word, e.g. `&ImageHeader::image_magic`.

  [image.h]: https://gitlab.com/starcopter/embedded/shared-utilities/blob/fec945c7b70909782e1753f89619292c9b06782e/sclib/image.h
  [crc32()]: https://docs.python.org/3/library/binascii.html#binascii.crc32

## Shared RAM

To communicate between applications (Bootloader, App Loader, Main App) and catch reboot loops, a block in RAM is set aside as shared RAM,
which is not re-initialized after a reboot and thus can relay information from one application to another, even in the case of a system reset.

> **Note:** as long as the RAM remains powered it will keep its content, even in case of a system reset

```
0x20008000 ┌──────────────────────┐
           │                      │         Application RAM (stack, heap, etc.)
           .       31.5 KiB       . ◄─────  private to each application
           .   Application RAM    .         initialized on application boot
           │                      │
0x20000200 ├──────────────────────┤         Shared RAM persisted across reboots
           │ 512 Bytes Shared RAM │ ◄─────  Initialized to zero on cold boot
0x20000000 └──────────────────────┘
```

### Hot Boot :fire: vs. Cold Boot :snowflake:

Upon boot the bootloader accesses the _RCC clock control & status register_ `RCC->CSR` to query the reason for the system reset.

A few reset events are classified as a full reset (cold boot), causing the shared RAM to be reset as well.
All other reset events will be detected as a hot reboot and will leave the shared RAM section untouched.

| Event | Boot | Flags |
| ----- |:----:| ------- |
| Power on | :snowflake: | Brown-Out Reset |
| NRST Pin | :snowflake: | Pin Reset flag |
| OpenOCD `reset halt` | :fire: | Pin Reset and Software Reset |
| CMSIS `NVIC_SystemReset()` | :fire: | Pin Reset and Software Reset |
| after Option Byte Load | :snowflake: | Option-Byte Loader Reset |
| Watchdog | :fire: | Window Watchdog or Indepentent Watchdog Reset |

```c
uint32_t csr = RCC->CSR;
if (csr & (RCC_CSR_BORRSTF | RCC_CSR_OBLRSTF) || (csr & RCC_CSR_PINRSTF && !(csr & RCC_CSR_SFTRSTF))) {
    // Brown-Out Reset or Option-Byte Loader Reset or (Pin Reset and not Sofware Reset) --> cold boot
    memset(&__shared_ram_start__, 0, (size_t) &__shared_ram_end__ - (size_t) &__shared_ram_start__);
}
PERIPH_BIT(RCC->CSR, RCC_CSR_RMVF_Pos) = 1;
```

### Launch Context and Reboot Loops

If an application is valid (by CRC verification) but crashes directly after boot, it might cause the processor to be stuck in a reboot loop,
preventing it from launching a different, possibly more stable application instead.
A scenario like this has a good chance of bricking the device.

To circumvent this, both the bootloader and the [app loader][App Loader] keep a launch context in shared (persistent) RAM.
In that launch context struct, the _launching_ application increments a boot counter when starting an application.
The _launched_ application then can reset this boot counter once it has reached stable operation.
Apart from the boot count, the launch context keeps a pointer to the launched application.
That way, the _launching_ application can pick up where it left off, even if the launched application was not the first detected in the application space.

```c
struct ApplicationContext {
    const ImageHeader_t* application;
    uint32_t             boot_count;
};

struct SharedRAM {
    ApplicationContext_t  bootloader_launch_context;
    ApplicationContext_t  app_loader_launch_context;
    const ImageHeader_t*  caller;
    ApplicationContext_t* current_context;

    // [...]
};
```

### Inter-Application Commands

The main reason for the shared RAM is the possibility to communicate from one application to another (or itself through reboot).
Information is exchanged via a command (enum) and a command-specific argument struct, verified by a checksum over `command` and `param`.

```c
enum InterApplicationCommand {
    eINTER_APPLICATION_COMMAND_NONE = 0,
    // further commands
};

struct SharedRAM {
    // [...]

    uint32_t command;
    union Param {
        // one struct with arguments per command
    } param;
    uint32_t crc;
};
```

These inter-application commands are of no relevance to the bootloader.

#### DFU Request

When an application (either the main application or the app loader) receives and accepts a [UAVCAN firmware update request][COMMAND_BEGIN_SOFTWARE_UPDATE],
it stores the update context in shared RAM and triggers a software reset.
Since the reset is treated as a [hot boot :fire:](#hot-boot-fire-vs-cold-boot-snowflake), shared RAM persists and the request will be picked up by the [app loader][App Loader].

```c
struct DFU {
    char                 path[256];
    uint32_t             server_node_id;
    const ImageHeader_t* current_application;
} dfu;
```

  [COMMAND_BEGIN_SOFTWARE_UPDATE]: https://github.com/UAVCAN/public_regulated_data_types/blob/master/uavcan/node/435.ExecuteCommand.1.1.uavcan#L16..L47
