/**
 * @file device.h
 * @author Lasse Fröhner <lasse@starcopter.com>
 * @brief Load Correct CMSIS Device Header
 * @date 2021-12-01
 *
 * @copyright Copyright (c) 2021 starcopter GmbH
 *
 * This software is distributed under the terms of the MIT License.
 *
 * @see https://www.keil.com/pack/doc/CMSIS/Core/html/device_h_pg.html
 */

#if defined(STM32G431xx) || defined(STM32G441xx) || defined(STM32G471xx) || defined(STM32G473xx) ||                    \
    defined(STM32G474xx) || defined(STM32G484xx) || defined(STM32GBK1CB) || defined(STM32G491xx) ||                    \
    defined(STM32G4A1xx)
// STM32G4 series
#    include <stm32g4xx.h>
#    include <stm32g4xx-extra.h>
#else
#    error "Please declare your platform as a build time macro"
#endif
